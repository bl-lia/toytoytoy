//
//  RootViewController.swift
//  ToyToyToy
//
//  Created by yuki_fn on 12/8/14.
//  Copyright (c) 2014 volare. All rights reserved.
//

import UIKit

class RootViewController: UIViewController, UIPageViewControllerDelegate, UIScrollViewDelegate {
    
    var pageViewController: UIPageViewController?
    var scrollView: UIScrollView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.pageViewController = UIPageViewController(transitionStyle: .Scroll, navigationOrientation: .Horizontal, options: nil)
        self.pageViewController!.delegate = self
        
        let startingViewController: DataViewController = self.modelController.viewControllerAtIndex(0, storyboard: self.storyboard!)!
        let viewControllers: NSArray = [startingViewController]
        self.pageViewController!.setViewControllers(viewControllers, direction: .Forward, animated: true, completion: {done in })
        
        self.pageViewController!.dataSource = self.modelController
        
        self.addChildViewController(self.pageViewController!)
        self.view.addSubview(self.pageViewController!.view)
        
        // Set the page view controller's bounds using an inset rect so that self's view is visible around the edges of the pages.
        var pageViewRect = self.view.bounds
        self.pageViewController!.view.frame = pageViewRect
        
        self.pageViewController!.didMoveToParentViewController(self)
        
        // Add the page view controller's gesture recognizers to the book view controller's view so that the gestures are started more easily.
        self.view.gestureRecognizers = self.pageViewController!.gestureRecognizers
        
        NSNotificationCenter.defaultCenter().addObserverForName("ChangePage", object: nil, queue: NSOperationQueue.mainQueue()) { (notification) -> Void in
            let color = notification.userInfo!["color"] as UIColor
            
            startingViewController.view.backgroundColor = color
            let tempViewControllers: NSArray = [startingViewController]
            
            self.pageViewController?.setViewControllers(tempViewControllers, direction: .Forward, animated: true, completion: {done in })
        }
        
        for view in self.pageViewController!.view.subviews {
            if view.isKindOfClass(UIScrollView) {
                self.scrollView = view as? UIScrollView
                self.scrollView?.delegate = self
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var modelController: ModelController {
        // Return the model controller object, creating it if necessary.
        // In more complex implementations, the model controller may be passed to the view controller.
        if _modelController == nil {
            _modelController = ModelController()
        }
        return _modelController!
    }
    
    var _modelController: ModelController? = nil
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        //        let offset = self.view.frame.size.width - self.scrollView!.contentOffset.x
        let offset = ((self.scrollView!.contentOffset.x / self.view.frame.size.width) - 1) * -1
//        println("offset: \(offset)")
        let userInfo = ["offset" : offset]
        NSNotificationCenter.defaultCenter().postNotificationName("animateSwipe", object: nil, userInfo: userInfo)
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        NSNotificationCenter.defaultCenter().postNotificationName("scrollViewWillBeginDragging", object: nil, userInfo: nil)
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        NSNotificationCenter.defaultCenter().postNotificationName("scrollViewDidEndDragging", object: nil, userInfo: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
