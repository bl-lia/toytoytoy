//
//  ChannelTabViewController.swift
//  ToyToyToy
//
//  Created by yuki_fn on 12/8/14.
//  Copyright (c) 2014 volare. All rights reserved.
//

import UIKit

class ChannelTabViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    struct DragParam {
        var offset: CGFloat
        var startLoc: CGFloat
        var negativeLoc: CGFloat
        var positiveLoc: CGFloat
    }
    
    @IBOutlet var collectionView: UICollectionView!
    
    var cellColors = [UIColor]()
    var drag: DragParam?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        for (var i = 0; i<13; i++) {
            let r = Double(arc4random_uniform(255) + 1) / 255.0
            let g = Double(arc4random_uniform(255) + 1) / 255.0
            let b = Double(arc4random_uniform(255) + 1) / 255.0
            let color = UIColor(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: 1.0)
            
            self.cellColors.append(color)
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "animate:", name: "animateSwipe", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "beginDragging:", name: "scrollViewWillBeginDragging", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "endDragging:", name: "scrollViewDidEndDragging", object: nil)
        
    }
    
    func endDragging(notification: NSNotification) {
//        println("end dragging")
    }
    
    func beginDragging(notification: NSNotification) {
        let centerX = self.collectionView.frame.width / 2
        let centerY = self.collectionView.frame.height / 2
        
        let indexPath = self.collectionView.indexPathForItemAtPoint(CGPointMake(centerX, centerY))
        
        let currentCell = self.collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as UICollectionViewCell
        let negativeCell = self.collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: NSIndexPath(forRow: indexPath!.row - 1, inSection: indexPath!.section)) as UICollectionViewCell
        let positiveCell = self.collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: NSIndexPath(forRow: indexPath!.row + 1, inSection: indexPath!.section)) as UICollectionViewCell
        
        self.drag = DragParam(offset: 0.0, startLoc: currentCell.center.x, negativeLoc: currentCell.center.x - (positiveCell.center.x - currentCell.center.x), positiveLoc: currentCell.center.x + (currentCell.center.x - negativeCell.center.x))
    }
    
    func animate(notification: NSNotification) {
//        let userInfo = notification.userInfo
//        let screenWidth = UIScreen.mainScreen().bounds.size.width
//        let offset = userInfo!["offset"] as CGFloat
//        println("channel offset: \(offset)")
        
//        let centerX = self.collectionView.frame.width / 2
//        let centerY = self.collectionView.frame.height / 2
//        
//        let indexPath = self.collectionView.indexPathForItemAtPoint(CGPointMake(centerX, centerY))
//        
//        println("\(indexPath)")
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        
        let userInfo = notification.userInfo
        let offset = userInfo!["offset"] as CGFloat
        
        self.drag?.offset = offset
        if self.drag?.offset == 0.0 {
            self.drag = nil
        }
        
        var move: CGFloat
        
        if let drag = self.drag {
            
            if offset > 0 {
                move = (drag.positiveLoc - drag.startLoc) * offset + drag.startLoc
            } else {
                move = (drag.negativeLoc - drag.startLoc) * (offset * -1) + drag.startLoc
            }
            
            let o = CGPointMake(move - screenWidth/2, 0)
            
            self.collectionView.setContentOffset(o, animated: true)
            
            println("move: \(move)")
            println("offset: \(offset)")
        }
        
        
        
//        println("\(self.drag?.offset):\(self.drag?.startLoc):\(self.drag?.negativeLoc):\(self.drag?.positiveLoc)")
        
    }
    
    override func viewWillAppear(animated: Bool) {
        self.collectionView.setNeedsLayout()
        self.collectionView.layoutIfNeeded()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        let first = NSIndexPath(forItem: 500, inSection: 0)
        self.collectionView.scrollToItemAtIndexPath(first, atScrollPosition: UICollectionViewScrollPosition.CenteredHorizontally, animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 13 * 1000
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as UICollectionViewCell
        
        let color = self.cellColors[indexPath.row % 13]
        
        cell.backgroundColor = color
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let screenWidth = UIScreen.mainScreen().bounds.size.width
        self.collectionView.contentInset = UIEdgeInsetsMake(0, screenWidth / 2, 0, screenWidth / 2)

        let cell = self.collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as UICollectionViewCell
        let offset = CGPointMake(cell.center.x - screenWidth / 2, 0)
        self.collectionView.setContentOffset(offset, animated: true)
        
        NSNotificationCenter.defaultCenter().postNotificationName("ChangePage", object: nil, userInfo: ["color": self.cellColors[indexPath.row % 13]])
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
//        let width = self.collectionView.contentSize.width
//        let offset = self.collectionView.contentOffset.x
//        let screenWidth = UIScreen.mainScreen().bounds.size.width
//        
//        let margin = offset / (width - screenWidth)
//        
//        if margin > 0.9 {
//            self.setupAsInfinite(true)
//        } else if margin < 0.1 {
//            self.setupAsInfinite(false)
//        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
